package de.nuclearsunrise.javamsng.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JavaMsNgApplicationTests {

	@LocalServerPort
	private Integer port;

	@Autowired
	private TestRestTemplate rt;

	@Test
	public void testMain() {
		JavaMsNgApplication.main(new String[0]);
	}

	@Test
	public void testStatus() {
		String response = this.rt.getForObject("http://localhost:" + this.port + "/health", String.class);
		assertThat(response).isEqualTo("running");
	}

	@Test
	public void testJavaVersion() {
		JavaVersionController.ResponseRO response = this.rt.getForObject("http://localhost:" + this.port + "/javaversion", JavaVersionController.ResponseRO.class);
		assertThat(response.javaVersion).startsWith("11.");
		assertThat(response.userTimezone).isNotEmpty();
		assertThat(response.javaRuntimeName).startsWith("OpenJDK");
		assertThat(response.javaRuntimeVersion).startsWith("11.");
		assertThat(response.javaVmName).startsWith("OpenJDK");
		assertThat(response.javaVmVersion).startsWith("11.");
	}

}
