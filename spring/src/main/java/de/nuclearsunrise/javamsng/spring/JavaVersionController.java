package de.nuclearsunrise.javamsng.spring;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JavaVersionController {

    @GetMapping("/javaversion")
    public ResponseRO getVersion() {
        ResponseRO response = new ResponseRO();
        response.javaVersion = System.getProperty("java.version");
        response.userTimezone = System.getProperty("user.timezone");
        response.javaRuntimeName = System.getProperty("java.runtime.name");
        response.javaRuntimeVersion = System.getProperty("java.runtime.version");
        response.javaVmName = System.getProperty("java.vm.name");
        response.javaVmVersion = System.getProperty("java.vm.version");
        return response;
    }

    public static class ResponseRO {
        @JsonProperty("java.version")
        public String javaVersion;
        @JsonProperty("user.timezone")
        public String userTimezone;
        @JsonProperty("java.runtime.name")
        public String javaRuntimeName;
        @JsonProperty("java.runtime.version")
        public String javaRuntimeVersion;
        @JsonProperty("java.vm.name")
        public String javaVmName;
        @JsonProperty("java.vm.version")
        public String javaVmVersion;
    }

}
