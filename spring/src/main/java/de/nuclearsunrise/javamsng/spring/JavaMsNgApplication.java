package de.nuclearsunrise.javamsng.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaMsNgApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaMsNgApplication.class, args);
	}

}
