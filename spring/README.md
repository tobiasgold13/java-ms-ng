# Spring Boot

This will be a standard gradle project with the spring boot frameworks.

## Application
It's just a dummy application for testing purposes. There won't be any business logic or something like that.
The following endpoints can be used:
+ */health* just a quick health check, that returns *running* if the app is running
+ */javaversion* returns some information about the jvm the app is running in. Should not be done in a production app, because it might make it more vulnerable.

## Test coverage
Testing is important and even more important in cloud projects with automated builds. The test coverage is calculated during gradle build with the JaCoCo Plugin.
A coverage report can be generated with the following task (you need to build before):
```
gradle jacocoTestReport
```
The report will be placed in the build folder of the project:
```
./build/reports/jacoco/test/html/index.html
```

## Docker
In the Dockerfile the jar is build inside a gradle image and copied to a smaller image with just the jre.

The following Dockerfiles exist, to build the image in different ways:

| Dockerfile name | Info |
| --------------- | ---- |
| Dockerfile_alpine | builds the app inside a gradle image and copies the runtime jar to a smaller image with just the jre. |
| Dockerfile_jlink | creates a custom JVM with jlink, should result in a smaller image |
