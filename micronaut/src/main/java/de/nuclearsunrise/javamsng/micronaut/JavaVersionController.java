package de.nuclearsunrise.javamsng.micronaut;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller
public class JavaVersionController {

    @Get("/javaversion")
    public ResponseRO getVersion() {
        ResponseRO response = new ResponseRO();
        response.javaVersion = System.getProperty("java.version");
        response.userTimezone = System.getProperty("user.timezone");
        response.javaRuntimeName = System.getProperty("java.runtime.name");
        response.javaRuntimeVersion = System.getProperty("java.runtime.version");
        response.javaVmName = System.getProperty("java.vm.name");
        response.javaVmVersion = System.getProperty("java.vm.version");
        return response;
    }

    @Introspected
    public static class ResponseRO {
        @JsonProperty("java.version")
        public String javaVersion;
        @JsonProperty("user.timezone")
        public String userTimezone;
        @JsonProperty("java.runtime.name")
        public String javaRuntimeName;
        @JsonProperty("java.runtime.version")
        public String javaRuntimeVersion;
        @JsonProperty("java.vm.name")
        public String javaVmName;
        @JsonProperty("java.vm.version")
        public String javaVmVersion;
    }

}
