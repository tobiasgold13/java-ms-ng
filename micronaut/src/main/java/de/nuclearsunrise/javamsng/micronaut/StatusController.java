package de.nuclearsunrise.javamsng.micronaut;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

@Controller
public class StatusController {

    @Get("/health")
    @Produces(MediaType.TEXT_PLAIN)
    public String health() {
        return "running";
    }

}
