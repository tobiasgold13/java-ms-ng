package de.nuclearsunrise.javamsng;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@MicronautTest
public class StatusTest {

    @Inject
    @Client("/")
    private RxHttpClient client;

    @Test
    public void testHealth() {
        MutableHttpRequest<Object> request = HttpRequest.GET("/health");
        String response = client.toBlocking().retrieve(request);

        assertNotNull(response);
        assertEquals(response, "running");
    }

}
