package de.nuclearsunrise.javamsng;

import de.nuclearsunrise.javamsng.micronaut.JavaVersionController;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@MicronautTest
public class JavaVersionTest {

    @Inject
    @Client("/")
    private RxHttpClient client;

    @Test
    public void testJavaVersion() {
        MutableHttpRequest<Object> request = HttpRequest.GET("/javaversion");
        JavaVersionController.ResponseRO response = client.toBlocking().retrieve(request, JavaVersionController.ResponseRO.class);

        assertNotNull(response);
        // assertNotNull(response.javaVersion);
        // assertTrue(response.javaVersion.startsWith("11."));
        // assertNotNull(response.userTimezone);
        // assertNotNull(response.javaRuntimeName);
        // assertTrue(response.javaRuntimeName.startsWith("OpenJDK"));
        // assertNotNull(response.javaRuntimeVersion);
        // assertTrue(response.javaRuntimeVersion.startsWith("11."));
        // assertNotNull(response.javaVmName);
        // assertTrue(response.javaVmName.startsWith("OpenJDK"));
        // assertNotNull(response.javaVmVersion);
        // assertTrue(response.javaVmVersion.startsWith("11."));
    }

}
