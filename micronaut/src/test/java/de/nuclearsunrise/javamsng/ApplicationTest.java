package de.nuclearsunrise.javamsng;

import de.nuclearsunrise.javamsng.micronaut.Application;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@MicronautTest
public class ApplicationTest {

    @Test
    public void testMain() {
        Application.main(new String[0]);
    }

    @Test
    public void testClass() {
        Application application = new Application();
        assertNotNull(application);
    }

}
