# Micronaut / GraalVM

This will be a standard gradle project with the micronaut framework, intended to be compiled to a native application with GraalVM.

## Micronaut

What is the micronaut framework?

What [micronaut.io](https://micronaut.io/) says:
> A modern, JVM-based, full-stack framework for building modular, easily testable microservice and serverless applications.

I used for building a simple dummy microservice, with a few REST-Request mappings and simple stuff similar to that.
Micronaut does not use reflection, which is very important to what I wanted to do with the application: Compile it to a native executable with the help of the GraalVM project.

## GraalVM

So, what is GraalVM?

Well, let [graalvm.org](https://www.graalvm.org/) speak for me:
> GraalVM is a universal virtual machine for running applications written in JavaScript, Python, Ruby, R, JVM-based languages like Java, Scala, Kotlin, Clojure, and LLVM-based languages such as C and C++.

It also offers:
> Native images compiled with GraalVM ahead-of-time improve the startup time and reduce the memory footprint of JVM-based applications.

That's is what is really interesting: Compile our old, boring and probably slow java application to a native executable. Hopefully it will be much smaller and faster than a "fat jar" with a jvm runtime.

## Application
It's just a dummy application for testing purposes. There won't be any business logic or something like that.
The following endpoints can be used:
+ */health* just a quick health check, that returns *running* if the app is running
+ */javaversion* returns some information about the jvm the app is running in. Should not be done in a production app, because it might make it more vulnerable.

Pretty exited, what the */javaversion* mapping will return, when compiled to a native executable.

## Test coverage
Of course there are jUnit tests covering the source code and the test couverage is calculated with the JaCoCo gradle plugin.
The tests are executed during the build and the minimum test coverage is checked (*see build.gradle*).

A coverage report can be generated with the following task (you need to build before):
```
gradle jacocoTestReport
```
The report will be placed in the build folder of the project:
```
./build/reports/jacoco/test/html/index.html
```

## Docker
Docker multi-stage builds are used here:

1. build a regular "fat jar" with everything needed to run the application as a normal java application.
2. the resulting jar file is compiled to a static-linked native executable using graalvm. (I had to specify the --static option, to make the resulting binary run in the alpine docker image later)
3. put the compiled binary in an alpine docker container and enjoy the small docker image result with our boring old java application running blazingly fast!!

## TODO
I don't really now the micronaut framework, so here are some additional links to read:

+ https://micronaut-projects.github.io/micronaut-test/latest/guide/index.html
+ https://guides.micronaut.io/micronaut-creating-first-graal-app/guide/index.html

There is some collaboration between micronaut and spring, so maybe this will be of interest:

+ https://www.infoq.com/news/2018/12/micronaut-for-spring-graalvm
+ https://github.com/micronaut-projects/micronaut-spring
+ https://micronaut-projects.github.io/micronaut-spring/latest/guide/index.html

There is another project called [Quarkus](https://quarkus.io/) that has something to do with GraalVM. Maybe I should check it out.....